const cheerio = require('cheerio');
const axios = require('axios');
const _ = require('lodash');
const Entities = require('html-entities').XmlEntities;
const Sequelize = require('sequelize');
const sequelize = new Sequelize('myname', 'netApp', '123456', {
    host: '35.221.255.45',
    dialect: 'mysql',
    dialectOptions: {
        charset: 'utf8_general_ci',
    },
    operatorsAliases: false,


    pool: {
        max: 16,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

const Name = sequelize.define('name', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    day: { type: Sequelize.INTEGER },
    gender: { type: Sequelize.INTEGER },
    firstName: { type: Sequelize.STRING },
    mean: { type: Sequelize.STRING },
    spell: { type: Sequelize.STRING },

});

// force: true will drop the table if it already exists
// Name.sync({force: true}).then(() => {
//     // Table created
//     return true;
// });


async function getMean(uri) {
    try {
        const response = await axios.get(uri);
        if (response.status === 200){
            let html = response.data;
            let $ = cheerio.load(html);
            let body = $('font').html();
            let arr = body.split(' <br> ');
            const entities = new Entities();
            let startA = arr[0].lastIndexOf(' ') + 1;
            let startB = arr[1].lastIndexOf(' ') + 1;
            let a = entities.decode(arr[0].substring(startA));
            let b = entities.decode(arr[1].substring(startB, arr['1'].length-1));

            return [a,b];
        }
    } catch (error) {
        console.error(error);
    }
}
async function getBody() {
    let pageCount = 1;
    let maxPage = 0;
    let dayCount = 7;
    try {
        // for(let dayCount = 1; dayCount <= 8; dayCount++) {
            for (let genderCount = 1; genderCount <= 2; genderCount++){
                do {
                    let uri = 'http://www.xn--42cfal7c0d4a1d7a3d8ji.com/index.php?page=' + pageCount + '&selday=' + dayCount + '&selx=' + genderCount;
                    const response = await axios.get(uri);
                    if (response.status === 200){
                        let html = response.data;
                        let $ = cheerio.load(html);
                        let post = $('div .post').html().toString();
                        let start1 = post.indexOf('<a href="http://www.xn--42cfal7c0d4a1d7a3d8ji.com/index.php?id=');
                        let end1 = post.indexOf('<center>');
                        let start2 = post.indexOf('<p align="center">');
                        let end2 = post.indexOf(',  </p>');
                        let page = post.substring(start2, end2);
                        let pages = page.split('  ,  ');

                        post = post.substring(start1, end1);
                        let a = post.split('|');
                        maxPage = pages.length;

                        for (let i = 0; i < a.length; i++){
                            let item = a[i];

                            $ = cheerio.load(item);
                            let name = $('a').text().trim();
                            if (!_.isEmpty(name)){
                                let start = item.indexOf('http:');
                                let end = item.indexOf('" title');
                                let link = item.substring(start, end);
                                let mean = await getMean(link);

                                await Name.findOrCreate({where: {firstName: name, day: dayCount, gender: genderCount}, defaults: {mean: mean[1], spell: mean[0],}})
                                    .spread((name, created) => {
                                        // console.log(name.get({
                                        //     plain: true
                                        // }));
                                        // console.log(created);
                                        console.log(dayCount);
                                    });
                            }
                        }
                    }
                    pageCount++;
                }while (pageCount <= maxPage)
                pageCount = 1;
            }
        // }
    } catch (error) {
        console.error(error);
    }

}

async function seed() {
    await getBody();
}
seed();

