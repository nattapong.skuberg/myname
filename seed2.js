const cheerio = require('cheerio');
const axios = require('axios');
const _ = require('lodash');
const Entities = require('html-entities').XmlEntities;
const Promise = require("bluebird");
const Sequelize = require('sequelize');

const entities = new Entities();
const sequelize = new Sequelize('myname', 'netApp', '123456', {
    host: '35.221.255.45',
    dialect: 'mysql',
    dialectOptions: {
        charset: 'utf8_general_ci',
    },
    operatorsAliases: false,


    pool: {
        max: 16,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

const GoodName = sequelize.define('good_name', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    firstName: { type: Sequelize.STRING },
    spell: { type: Sequelize.STRING },
    mean: { type: Sequelize.STRING },
});
const SacredName = sequelize.define('sacred_name', {
    id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
    name_id: { type: Sequelize.INTEGER },
    gender: { type: Sequelize.INTEGER },
    day: { type: Sequelize.INTEGER },
});

// force: true will drop the table if it already exists
// GoodName.sync({force: true}).then(() => {
//     // Table created
//     return true;
// });
// SacredName.sync({force: true}).then(() => {
//     // Table created
//     return true;
// });
const mode = 2;
main();
// getNameByCategory({
//     type: 1,
//     text: 'ก',
//     href: 'https://www.wordyguru.com/a/ชื่อมงคล/list/ก',
// });

async function main() {
    try {
        let temp = ['ก','ข','ค','ง','จ','ฉ','ช','ซ','ฌ','ญ','ฐ',
            'ฑ', 'ฒ', 'ณ','ด','ต','ถ','ท','ธ','น','บ','ป','ผ','ฝ','พ',
            'ฟ','ภ','ม','ย','ร','ล','ว','ศ','ษ','ส','ห','ฬ','อ','ฮ'];

        let temp2 = [
            'ชื่อผู้ชายที่เกิดวันอาทิตย์',
            'ชื่อผู้ชายที่เกิดวันจันทร์',
            'ชื่อผู้ชายที่เกิดวันอังคาร',
            'ชื่อผู้ชายที่เกิดวันพุธกลางวัน',
            'ชื่อผู้ชายที่เกิดวันพุธกลางคืน',
            'ชื่อผู้ชายที่เกิดวันพฤหัสบดี',
            'ชื่อผู้ชายที่เกิดวันศุกร์',
            'ชื่อผู้ชายที่เกิดวันเสาร์',

            'ชื่อผู้หญิงที่เกิดวันอาทิตย์',
            'ชื่อผู้หญิงที่เกิดวันจันทร์',
            'ชื่อผู้หญิงที่เกิดวันอังคาร',
            'ชื่อผู้หญิงที่เกิดวันพุธกลางวัน',
            'ชื่อผู้หญิงที่เกิดวันพุธกลางคืน',
            'ชื่อผู้หญิงที่เกิดวันพฤหัสบดี',
            'ชื่อผู้หญิงที่เกิดวันศุกร์',
            'ชื่อผู้หญิงที่เกิดวันเสาร์',
        ];
        let categories = [];

        for (let i = 0; i < temp2.length; i++){
            let gender = (i < 8)? 1 : 2;
            let day = (i < 8)? i + 1 : i - 7;
            categories.push({
                type: 2,
                text: temp2[i],
                href: 'https://www.wordyguru.com/a/ชื่อมงคล/list/' + temp2[i],
                gender: gender,
                day: day,
            });
        }
        // let uri = 'https://www.wordyguru.com/a/ชื่อมงคล/list';
        // const response = await axios.get(encodeURI(uri));
        // if (response.status === 200){
        //     let html = response.data;
        //     let $ = cheerio.load(html);
        //     $('.list li').each(function (index, item) {
        //         let a = $($(this).html());
        //         let category = {
        //             type: (a.text().length == 1)? 1 : 2,
        //             text: a.text(),
        //             href: a.attr('href'),
        //         };
        //
        //         categories.push(category);
        //     });
        //
        //     console.log(categories);
        //     await Promise.map(categories, function(category) {
        //         if (mode === category.type){
        //             return getNameByCategory(category);
        //         }
        //     });
        // }
            await Promise.map(categories, function(category) {
                if (mode === category.type){
                    return getNameByCategory(category);
                }
            });

    }
    catch (e) {
        console.log(e);
    }
}

async function getNameByCategory(category) {
    let i = 1;
    let n = 0;
    do {
        const response = await axios.get(encodeURI(category.href + '?page=' + i));
        if (response.status === 200){
            let html = response.data;
            let $ = cheerio.load(html);
            // $('ul.list-unstyled li.media').each(function (index, item) {
            //     let data = $(this).children('.media-body').html().toString().split('<br>');
            //     let name = {
            //         firstName: $(this).children('.media-body').children('h3').children('a').text().split('\n')[1],
            //         spell: entities.decode(data[3]),
            //         mean: entities.decode(data[1]),
            //     };
            // });

            let m = $('ul.list-unstyled li.media').length;
            for (let j = 0; j < m; j++){
                let item = $('ul.list-unstyled li.media').eq(j);
                let data = item.children('.media-body').html().toString().split('<br>');
                let name = {
                    firstName: item.children('.media-body').children('h3').children('a').text().split('\n')[1],
                    spell: entities.decode(data[3]),
                    mean: entities.decode(data[1]),
                };

                await GoodName.findOrCreate({where: {firstName: name.firstName}, defaults: {mean: name.mean, spell: name.spell,}})
                    .spread(async (name, created) => {
                        console.log(name.id);
                        console.log(name.firstName);

                        await SacredName.findOrCreate({where: {name_id: name.id, gender: category.gender, day: category.day}}).spread((sacredName, created2) => {
                            console.log(sacredName.id);
                            console.log(sacredName.name_id);
                        });
                    });
            }

            if (i === 1){
                n = $('ul.pagination li.page-item').length;
            }
        }
        i++;
    }while (i < n)
}